const express = require("express");
const bodyParser = require("body-parser");
const pgp = require("pg-promise")( /*options*/ );
const config = require("config");
const cors = require('cors');
const nodemailer = require('nodemailer')

const app = express();
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const jsonParser = express.json();

const PORT = config.get('port') || 3000
const postgresURI = config.get('postgresUri')

app.set("view engine", "hbs");

async function start() {
    try {
        const db = (await pgp(postgresURI));

        app.use(cors());

        // Роутер главной страницы
        app.get("/", function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');

            db.manyOrNone('SELECT id, title, place, count, time, date, price, "imageSrc" FROM "Events"')
                .then(function(data) {
                    res.send(data);
                })
                .catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        app.get("/events", function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');

            db.manyOrNone('SELECT * FROM "Events"')
                .then(function(data) {
                    res.send(data);
                })
                .catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        app.get("/eventProduct/:id", function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');

            let id = req.params.id;

            db.oneOrNone(`SELECT * FROM "Events" where id='${id}'`)
                .then(function(data) {
                    res.send(data);
                })
                .catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        app.post("/buy", jsonParser, function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');


            let mas = Array.from(req.body[0]);

            let name = req.body[1];
            let phone = req.body[2];

            let msg = `<p>Имя - ${name}</p><p>Телефон - ${phone}</p><table cellpadding='10px' border='1'><tr><td>Название</td><td>Цена</td></tr>`;

            mas.forEach((current) => {
                msg += `<tr><td>${current.title}</td><td>${current.price}</td></tr>`;
            });

            msg += "</table>";

            try {
                let transporter = nodemailer.createTransport({
                    host: 'smtp.mail.ru',
                    port: 465,
                    secure: true,
                    auth: {
                        user: 'moss.decor@inbox.ru',
                        pass: 'AndrewDimas2020',
                    },
                })

                transporter.sendMail({
                        from: 'moss.decor@inbox.ru',
                        to: 'moss.decor@inbox.ru',
                        subject: 'Сообщение с сайта',
                        html: msg
                    })
                    // let result = await 
                    // console.log(result);
            } catch (e) {
                return console.log('Error: ' + e.name + ":" + e.message);
            }

            res.send();

            // return console.log('Error: ' + e.name + ":" + e.message);
        })

        // определяем Routers
        const eventsRouter = express.Router();
        const certificatesRouter = express.Router();
        const corporatesRouter = express.Router();

        // Здесь обработчики для /events

        // ..


        // Здесь обработчики для /certificates

        app.get("/certificates", function(req, res) {
            res.set('Access-Control-Allow-Origin', '*');

            db.manyOrNone('SELECT * FROM "Certificates"')
                .then(function(data) {
                    res.send(data);
                })
                .catch(function(error) {
                    console.log("ERROR:", error);
                });
        })

        // ..


        // Здесь обработчики для /corporates
        // ..


        // сопотавляем роутеры
        app.use("/events", eventsRouter);
        app.use("/certificates", certificatesRouter);
        app.use("/corporates", corporatesRouter);



        app.listen(PORT, function() {
            console.log("Сервер успешно запущен, ожидает запросов...");
        });
    } catch (e) {
        console.log("Server error", e.message);
        process.exit(1);
    }
}

start();