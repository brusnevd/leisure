import { SETEVENTS, SETCERTIFICATES, SETBASKET } from "./types";

const initialState = {
    events: [],
    certificates: [],
    corporates: [],
    basket: []
}

export default function rootReducer(state = initialState, action) {
    switch (action.type) {
        case SETEVENTS:
            return {
                ...state,
                events: action.payload
            }
        case SETCERTIFICATES:
            return {
                ...state,
                certificates: action.payload
            }
        case SETBASKET:
            return {
                ...state,
                basket: action.payload
            }
        default:
            return state;
    }
}