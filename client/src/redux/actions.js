import { SETCERTIFICATES, SETEVENTS, SETBASKET } from "./types";

export function setEvents(state) {
    return {
        type: SETEVENTS,
        payload: state
    }
}

export function setCertificates(state) {
    return {
        type: SETCERTIFICATES,
        payload: state
    }
}

export function setBasket(state) {
    return {
        type: SETBASKET,
        payload: state
    }
}