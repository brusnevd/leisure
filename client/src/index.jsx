import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import {createStore} from "redux";
import {Provider} from "react-redux";
import rootReducer from "./redux/rootReducer.js";
import showBasket from './custom/basket.js';

import "./reset.scss";

let store = createStore(rootReducer);

const app = (
    <Provider store={store}>
        <App store={store} showBasket={showBasket} />
    </Provider>
)

ReactDOM.render(
    app,
    document.getElementById('root')
);