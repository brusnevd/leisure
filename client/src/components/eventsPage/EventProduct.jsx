import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";

import "./product.scss";

function EventProduct(props) {
    let state = props.store.getState().events;

    let urlId = props.match.params.id;

    let product;

    for (let i = 0; i < state.length; i++) {
        if (state[i].id.toString() == urlId.toString()) {
            product = state[i];
        }
    }
    
    if (product == undefined) {
        return (
            <div className="">Ошибка 404</div>
        )
    }

    let stateCopy = state.slice();

    stateCopy = stateCopy.filter((c) => {
        if (c.id.toString() == urlId.toString()) {
            return false;
        } else {
            return true;
        }
    });

    if (stateCopy.length > 4) {
        stateCopy = stateCopy.slice(0, 4);
    }

    let description = product.description + "<p class='order-paragraph'><b>Для записи оставьте заявку или пишите/звоните +375 44 70-30-226 (Viber, Telegram)</b></p><p class='dop-image'><img src='/public/img/events/" + product.dopImage +"'></p>";

    description = {__html: description};

    let recommended = stateCopy.map((current) => {
        return (
            <div className="card" key={current.id}>
                {/* <div className="image"> */}
                    <img src={"/public/img/events/" + current.imageSrc} alt=""/>
                {/* </div> */}
                <p className="title">{current.title}</p>
                <div className="info">
                    <div className="place">
                        <img src="/public/img/map-marker.png" alt=""/>
                            <p>{current.place}</p>
                    </div>
                    <div className="count">
                        <img src="/public/img/person.png" alt=""/>
                        <p>Количество мест - {current.count}</p>
                    </div>
                    <div className="duration">
                        <img src="/public/img/clock.png" alt=""/>
                        <p>{current.time}</p>
                    </div>
                    <div className="date">
                        <img src="/public/img/calendar.png" alt=""/>
                        <p>{current.date}</p>
                    </div>
                </div>
                <div className="footer-info">
                    <div className="price">{current.price}</div>
                    <NavLink to={'/eventProduct/' + current.id}>
                        <div className="footer-button">Подробнее</div>
                    </NavLink>
                </div>
            </div>
        )
    });

    return (
        <div className="">
            <div className="event-product">
                <div className="title">Описание события</div>
                <div className="content" dangerouslySetInnerHTML={description}></div>
            </div>
            <div className="left-order">
                <div className="title">Оставить заявку</div>
                <div className="form">
                    <input type="text" className="form-name" placeholder="Ваше имя" />
                    <input type="text" className="form-phone" placeholder="Ваш телефон" />
                    <div className="button">Заказать</div>
                </div>
            </div>
            <div className="recommended-events">
                <div className="title">Может быть интересно</div>
                <div className="recommended-events-list">
                    {recommended}
                </div>
                <NavLink to={'/events'}>
                    <div className="btn-container"><div className="button">Больше событий</div></div>
                </NavLink>
            </div>
        </div>
    )
}

export default EventProduct;