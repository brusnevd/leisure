import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";

function filterEvents(event, props, reset) {
    fetch("http://localhost:3333/events")
        .then(res => res.json())
        .then(
            (result) => {
                props.setEvents(result);
                if (reset == undefined) {
                    let state = props.store.getState().events;
    
                    // Выбираем выбранные направления
                    let activeSpheres = Array.from(document.querySelectorAll(".spheres-item.active"));
    
                    activeSpheres = activeSpheres.map((cur) => {
                        return cur.innerText;
                    });
    
                    // Выбираем выбранную дату
                    let date = new Date(document.getElementById("date").value);
    
                    // Выбираем города
                    let towns = Array.from(document.querySelectorAll(".town-item"));
    
                    towns = towns.map((cur) => {
                        if (cur.querySelector("input[type='checkbox']").checked) {
                            return cur.querySelector("label").innerText;
                        }
                    });
    
                    towns = towns.filter((cur) => {
                        return cur != undefined;
                    });
    
                    // Определение цен
                    let minPrice = Number(document.getElementById("from").value);
                    let maxPrice = Number(document.getElementById("to").value);
    
                    let filteredEvents = state.filter((current) => {
                        let datemas = current.date.split(".");
                        let curDate = new Date(datemas[2] + "-" + datemas[1] + "-" + datemas[0]);
                        let result = true;
                        if (parseFloat(current.price) < minPrice || parseFloat(current.price) > maxPrice || !towns.includes(current.town) || !activeSpheres.includes(current.sphere)) {
                            result = false;
                        }
                        if (result == true) {
                            console.log(curDate.getDate(), date.getDate());
                            if (current.date != "Ежедневно") {
                                if (current.date != "Каждые выходные") {
                                    if (curDate.getFullYear() != date.getFullYear() || curDate.getMonth() != date.getMonth() || curDate.getDate() != date.getDate()) {
                                        result = false;
                                    }
                                }
                            }
                        }
                        return result;
                    });
    
                    props.setEvents(filteredEvents);
                } else {
                    let activeSpheres = Array.from(document.querySelectorAll(".spheres-item.active"));
                    activeSpheres.forEach((current) => {
                        current.classList.remove("active");
                    });
                    let towns = Array.from(document.querySelectorAll(".town-item"));
                    towns.forEach((cur) => {
                        cur.querySelector("input[type='checkbox']").checked = false;
                    });
                    document.getElementById("date").value = "";
                    document.getElementById("from").value = "100";
                    document.getElementById("to").value = "6500";

                }
                window.scrollTo({
                    top: 700,
                    behavior: 'smooth'
                });
            },
            (error) => {
                console.log(error);
            }
        )
}

function sphereClickHandler(event) {
    event.target.classList.toggle("active");
}

function EventsHead(props) {
    return (
        <div className="events-head">
            <NavLink to={'/'}>
                <div className="logo"><img src="/public/img/logo.png" alt=""/></div>
            </NavLink>
            <div className="title">События</div>
            <div className="params-container">
                <div className="title-container">
                    <div className="params-title">Подбор событий по параметрам</div>
                </div>
                <div className="params">
                    <div className="date">
                        <div className="title"><img src="/public/img/calendar.png" />Выберите дату</div>
                        <input type="date" id="date" />
                    </div>
                    <div className="vertical-line"></div>
                    <div className="town">
                        <div className="title"><img src="/public/img/marker.png" />Город</div>
                        <div className="town-list">
                            <div className="town-item">
                                <input type="checkbox" id="gomel" />
                                <label htmlFor="gomel">Гомель</label>
                            </div>
                            <div className="town-item">
                            <input type="checkbox" id="minsk" />
                                <label htmlFor="minsk">Минск</label>
                            </div>
                            <div className="town-item">
                                <input type="checkbox" id="grodno" />
                                <label htmlFor="grodno">Гродно</label>
                            </div>
                            <div className="town-item">
                                <input type="checkbox" id="vitebsk" />
                                <label htmlFor="vitebsk">Витебск</label>
                            </div>
                            <div className="town-item">
                                <input type="checkbox" id="brest" />
                                <label htmlFor="brest">Брест</label>
                            </div>
                            <div className="town-item">
                                <input type="checkbox" id="mogilev" />
                                <label htmlFor="mogilev">Могилёв</label>
                            </div>
                        </div>
                    </div>
                    <div className="vertical-line"></div>
                    <div className="price">
                        <div className="title"><img src="/public/img/coins.png" />Определите ценовой диапазон</div>
                        <div className="price-range">
                            <span>От </span> <input type="Number" min="10" max="6499" step="1" defaultValue="10" id="from" />
                            <span>До </span> <input type="Number" min="11" max="6500" step="1" defaultValue="6500" id="to" />
                        </div>
                    </div>
                    <div className="vertical-line"></div>
                    <div className="sphere">
                        <div className="title"><img src="/public/img/baloon.png" />Выберите направление</div>
                        <div className="spheres-list">
                            <div className="spheres-item" onClick={sphereClickHandler}>Полёты</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Прыжки</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Экскурсии</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Квадроциклы</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Байдарки</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Стрелять</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Конные прогулки</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Походы</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Игры</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Яхты</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Лыжи</div>
                        </div>
                    </div>
                </div>
                <div className="button-container">
                    <div className="button" onClick={(event) => {filterEvents(event, props)}}>Показать события</div>
                    <div className="button-reset" onClick={(event) => {filterEvents(event, props, true)}}>Сбросить</div>
                </div>
            </div>
        </div>
    )
}

export default EventsHead;