import React, { useState, useEffect } from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
import buy from '../../custom/buyProduct';

function EventProductHead(props) {
    // Здесь будет браться информация из БД
    const [isLoaded3, setIsLoaded3] = useState(false);

    // Посылаем запрос на сервер
    useEffect(() => {
        fetch("http://localhost:3333/events")
            .then(res => res.json())
            .then(
                (result) => {
                    props.setEvents(result);
                    setIsLoaded3(true);
                },
                (error) => {
                    console.log(error)
                    setIsLoaded3(true);
                }
            )
    }, [])
    // useEffect(() => {
    //     fetch("http://localhost:3333/eventProduct/" + props.match.params.id)
    //         .then(res => res.json())
    //         .then(
    //             (result) => {
    //                 props.setEvents([result]);
    //                 setIsLoaded3(true);
    //             },
    //             (error) => {
    //                 console.log(error)
    //                 setIsLoaded3(true);
    //             }
    //         )
    // }, [])

    if (!isLoaded3) {
        return (
            <div className="">Загрузка</div>
        )
    } else {
        let state = props.store.getState().events;

        let urlId = props.match.params.id;

        let product;

        for (let i = 0; i < state.length; i++) {
            console.log(state[i])
            if (state[i].id.toString() == urlId.toString()) {
                product = state[i];
            }
        }

        return (
            <div className="event-product-head" style={{backgroundImage: `url("/public/img/events/${product.imageSrc}")`, backgroundSize: "cover", backgroundPosition: "center", backgroundAttachment: "fixed"}}>
                <NavLink to={'/'}>
                    <div className="logo"><img src="/public/img/logo.png" alt=""/></div>
                </NavLink>
                <div className="title">{product.title}</div>
                <div className="info">
                    <div className="place">
                        <img src="/public/img/marker.png" alt=""/>
                        <p>{product.town}</p>
                    </div>
                    <div className="date">
                        <img src="/public/img/calendar.png" alt=""/>
                        <p>{product.date}</p>
                    </div>
                    <div className="time">
                        <img src="/public/img/clock.png" alt=""/>
                        <p>{product.time}</p>
                    </div>
                </div>
                <div className="price">{product.price}</div>
                <div className="button-container">
                    <div className="button" onClick={() => {buy(props.store, product)}}>Забронировать</div>
                </div>
            </div>
        )
    }
}

export default EventProductHead;