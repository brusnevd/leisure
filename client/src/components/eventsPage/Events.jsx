import React, { useState, useEffect } from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
import './events.scss';

function Events(props) {
    // Здесь будет браться информация из БД
    const [isLoaded2, setIsLoaded2] = useState(false);

    // Посылаем запрос на сервер
    useEffect(() => {
        fetch("http://localhost:3333/events")
            .then(res => res.json())
            .then(
                (result) => {
                    props.setEvents(result);
                    setIsLoaded2(true);
                },
                (error) => {
                    console.log(error)
                    setIsLoaded2(true);
                }
            )
    }, [])

    if (!isLoaded2) {
        return (
            <div className="">Загрузка</div>
        )
    } else {
        let state = props.store.getState().events;

        let cards = state.map((current) => {
            return (
                <div className="card" key={current.id}>
                    <div className="image">
                        <img src={"/public/img/events/" + current.imageSrc} alt=""/>
                    </div>
                    <div className="content">
                        <div className="info">
                            <div className="place">
                                <img src="/public/img/marker.png" alt=""/>
                                <p>{current.place}</p>
                            </div>
                            <div className="count">
                                <img src="/public/img/person.png" alt=""/>
                                <p>Количество мест - {current.count}</p>
                            </div>
                            <div className="duration">
                                <img src="/public/img/clock.png" alt=""/>
                                <p>{current.time}</p>
                            </div>
                            <div className="date">
                                <img src="/public/img/calendar.png" alt=""/>
                                <p>{current.date}</p>
                            </div>
                        </div>
                        <div className="title">
                            {current.title}
                        </div>
                    </div>
                    <div className="vertical-line"></div>
                    <div className="footer-info">
                        <div className="price">{current.price}</div>
                        <NavLink to={'/eventProduct/' + current.id}>
                            <div className="button">Подробнее</div>
                        </NavLink>
                    </div>
                </div>
            )
        })

        return (        
            <div className="events-page">
                <div className="title">Подобранные события</div>
                <div className="events-list">
                    {cards}
                </div>
            </div>
        )
    }
}

export default Events;