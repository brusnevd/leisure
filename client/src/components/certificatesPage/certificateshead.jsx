import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";

function filterEvents(event, props, reset) {
    fetch("http://localhost:3333/certificates")
        .then(res => res.json())
        .then(
            (result) => {
                props.setCertificates(result);
                if (reset == undefined) {
                    let state = props.store.getState().certificates;
    
                    // Выбираем выбранные направления
                    let activeSpheres = Array.from(document.querySelectorAll(".spheres-item.active"));
    
                    activeSpheres = activeSpheres.map((cur) => {
                        return cur.innerText;
                    });
    
                    // Выбираем выбранную дату
                    let seasons = Array.from(document.querySelectorAll(".season-item"));
    
                    seasons = seasons.map((cur) => {
                        if (cur.querySelector("input[type='checkbox']").checked) {
                            return cur.querySelector("label").innerText;
                        }
                    });

                    seasons.push("Любой");
    
                    // Выбираем города
                    let towns = Array.from(document.querySelectorAll(".town-item"));
    
                    towns = towns.map((cur) => {
                        if (cur.querySelector("input[type='checkbox']").checked) {
                            return cur.querySelector("label").innerText;
                        }
                    });
    
                    towns = towns.filter((cur) => {
                        return cur != undefined;
                    });
    
                    // Определение цен
                    let minPrice = Number(document.getElementById("from").value);
                    let maxPrice = Number(document.getElementById("to").value);
    
                    let filteredEvents = state.filter((current) => {
                        let result = true;
                        console.log(towns.includes(current.town), current.town, seasons.includes(current.date), current.date)
                        if (parseFloat(current.price) < minPrice || parseFloat(current.price) > maxPrice || !towns.includes(current.town) || !activeSpheres.includes(current.sphere) || !seasons.includes(current.date)) {
                            result = false;
                        }
                        return result;
                    });
    
                    props.setCertificates(filteredEvents);
                } else {
                    let seasons = Array.from(document.querySelectorAll(".season-item"));
                    seasons.forEach((cur) => {
                        cur.querySelector("input[type='checkbox']").checked = false;
                    });
                    let activeSpheres = Array.from(document.querySelectorAll(".spheres-item.active"));
                    activeSpheres.forEach((current) => {
                        current.classList.remove("active");
                    });
                    let towns = Array.from(document.querySelectorAll(".town-item"));
                    towns.forEach((cur) => {
                        cur.querySelector("input[type='checkbox']").checked = false;
                    });
                    document.getElementById("date").value = "";
                    document.getElementById("from").value = "100";
                    document.getElementById("to").value = "6500";

                }
                window.scrollTo({
                    top: 700,
                    behavior: 'smooth'
                });
            },
            (error) => {
                console.log(error);
            }
        )
}

function sphereClickHandler(event) {
    event.target.classList.toggle("active");
}

function CertificatesHead(props) {
    return (
        <div className="certificates-head">
            <NavLink to={'/'}>
                <div className="logo"><img src="/public/img/logo.png" alt=""/></div>
            </NavLink>
            <div className="title">Сертификаты</div>
            <div className="params-container">
                <div className="title-container">
                    <div className="params-title">Подбор сертификатов по параметрам</div>
                </div>
                <div className="params">
                    <div className="date">
                        <div className="title"><img src="/public/img/calendar.png" />Выберите поры года</div>
                        <div className="seasons-list">
                            <div className="season-item">
                                <input type="checkbox" id="vesna" />
                                <label htmlFor="vesna">Весна</label>
                            </div>
                            <div className="season-item">
                            <input type="checkbox" id="leto" />
                                <label htmlFor="leto">Лето</label>
                            </div>
                            <div className="season-item">
                                <input type="checkbox" id="zima" />
                                <label htmlFor="zima">Зима</label>
                            </div>
                            <div className="season-item">
                                <input type="checkbox" id="osen" />
                                <label htmlFor="osen">Осень</label>
                            </div>
                        </div>
                    </div>
                    <div className="vertical-line"></div>
                    <div className="town">
                        <div className="title"><img src="/public/img/marker.png" />Город</div>
                        <div className="town-list">
                            <div className="town-item">
                                <input type="checkbox" id="gomel" />
                                <label htmlFor="gomel">Гомель</label>
                            </div>
                            <div className="town-item">
                            <input type="checkbox" id="minsk" />
                                <label htmlFor="minsk">Минск</label>
                            </div>
                            <div className="town-item">
                                <input type="checkbox" id="grodno" />
                                <label htmlFor="grodno">Гродно</label>
                            </div>
                            <div className="town-item">
                                <input type="checkbox" id="vitebsk" />
                                <label htmlFor="vitebsk">Витебск</label>
                            </div>
                            <div className="town-item">
                                <input type="checkbox" id="brest" />
                                <label htmlFor="brest">Брест</label>
                            </div>
                            <div className="town-item">
                                <input type="checkbox" id="mogilev" />
                                <label htmlFor="mogilev">Могилёв</label>
                            </div>
                        </div>
                    </div>
                    <div className="vertical-line"></div>
                    <div className="price">
                        <div className="title"><img src="/public/img/coins.png" />Определите ценовой диапазон</div>
                        <div className="price-range">
                            <span>От </span> <input type="Number" min="10" max="6499" step="1" defaultValue="10" id="from" />
                            <span>До </span> <input type="Number" min="11" max="6500" step="1" defaultValue="6500" id="to" />
                        </div>
                    </div>
                    <div className="vertical-line"></div>
                    <div className="sphere">
                        <div className="title"><img src="/public/img/baloon.png" />Выберите впечатление</div>
                        <div className="spheres-list">
                            <div className="spheres-item" onClick={sphereClickHandler}>Квесты</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Творчество</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Активный отдых</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Полёты</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Прыжки</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Экскурсии</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Квадроциклы</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Байдарки</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Стрелять</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Конные прогулки</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Походы</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Игры</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Яхты</div>
                            <div className="spheres-item" onClick={sphereClickHandler}>Лыжи</div>
                        </div>
                    </div>
                </div>
                <div className="button-container">
                    <div className="button" onClick={(event) => {filterEvents(event, props)}}>Показать сертификаты</div>
                    <div className="button-reset" onClick={(event) => {filterEvents(event, props, true)}}>Сбросить</div>
                </div>
            </div>
        </div>
    )
}

export default CertificatesHead;