import React, { useState, useEffect } from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
import './certificates.scss';

function Certificates(props) {
    // Здесь будет браться информация из БД
    const [isLoaded4, setIsLoaded4] = useState(false);

    // Посылаем запрос на сервер
    useEffect(() => {
        fetch("http://localhost:3333/certificates")
            .then(res => res.json())
            .then(
                (result) => {
                    props.setCertificates(result);
                    setIsLoaded4(true);
                },
                (error) => {
                    console.log(error)
                    setIsLoaded4(true);
                }
            )
    }, [])

    if (!isLoaded4) {
        return (
            <div className="">Загрузка</div>
        )
    } else {
        let state = props.store.getState().certificates;

        let cards = state.map((current) => {
            return (
                <div className="card" key={current.id}>
                    <NavLink to={"/certificateProduct/" + current.id}>
                        <img className="background" src={"/public/img/certificates/" + current.imageSrc} alt=""/>
                        <div className="content">
                            <div className="info">
                                <div className="place">
                                    <img src="/public/img/marker.png" alt=""/>
                                    <p>{current.town}</p>
                                </div>
                                <div className="duration">
                                    <img src="/public/img/clock.png" alt=""/>
                                    <p>{current.time}</p>
                                </div>
                            </div>
                        </div>
                        <div className="footer-info">
                            <div className="title">
                                {current.title}
                            </div>
                            <div className="price">
                                {current.price}
                            </div>
                        </div>
                    </NavLink>
                </div>
            )
        })

        return (        
            <div className="certificates-page">
                <div className="title">Подобранные сертификаты</div>
                <div className="events-list">
                    {cards}
                </div>
            </div>
        )
    }
}

export default Certificates;