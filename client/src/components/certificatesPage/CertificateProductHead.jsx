import React, { useState, useEffect } from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
import buy from '../../custom/buyProduct';



function CertificateProductHead(props) {
    // Здесь будет браться информация из БД
    const [isLoaded5, setIsLoaded5] = useState(false);

    // Посылаем запрос на сервер
    useEffect(() => {
        fetch("http://localhost:3333/certificates")
            .then(res => res.json())
            .then(
                (result) => {
                    props.setCertificates(result);
                    setIsLoaded5(true);
                },
                (error) => {
                    console.log(error)
                    setIsLoaded5(true);
                }
            )
    }, [])

    if (!isLoaded5) {
        return (
            <div className="">Загрузка</div>
        )
    } else {
        let state = props.store.getState().certificates;

        let urlId = props.match.params.id;

        let product;

        for (let i = 0; i < state.length; i++) {
            if (state[i].id.toString() == urlId.toString()) {
                product = state[i];
            }
        }

        return (
            <div className="certificate-product-head" style={{backgroundImage: `url("/public/img/certificates/${product.imageSrc}")`, backgroundSize: "cover", backgroundPosition: "center", backgroundAttachment: "fixed"}}>
                <NavLink to={'/'}>
                    <div className="logo"><img src="/public/img/logo.png" alt=""/></div>
                </NavLink>
                <div className="title">{product.title}</div>
                <div className="info">
                    <div className="place">
                        <img src="/public/img/marker.png" alt=""/>
                        <p>{product.town}</p>
                    </div>
                    <div className="time">
                        <img src="/public/img/clock.png" alt=""/>
                        <p>{product.time}</p>
                    </div>
                </div>
                <div className="price">{product.price}</div>
                <div className="button-container">
                    <div className="button" onClick={() => {buy(props.store, product)}}>Купить</div>
                </div>
            </div>
        )
    }
}

export default CertificateProductHead;