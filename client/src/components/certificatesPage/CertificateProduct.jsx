import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
import buy from '../../custom/buyProduct';

import "./product.scss";

function CertificateProduct(props) {
    let state = props.store.getState().certificates;

    let urlId = props.match.params.id;

    let product;

    for (let i = 0; i < state.length; i++) {
        if (state[i].id.toString() == urlId.toString()) {
            product = state[i];
        }
    }
    
    if (product == undefined) {
        return (
            <div className="">Ошибка 404</div>
        )
    }

    let stateCopy = state.slice();

    stateCopy = stateCopy.filter((c) => {
        if (c.id.toString() == urlId.toString()) {
            return false;
        } else {
            return true;
        }
    });

    if (stateCopy.length > 4) {
        stateCopy = stateCopy.slice(0, 4);
    }

    let description = "<p>" + product.description + "</p><p class='order-paragraph'><b>Для покупки оставьте заявку или пишите/звоните +375 44 70-30-226 (Viber, Telegram)</b></p>";

    description = {__html: description};

    let recommended = stateCopy.map((current) => {
        return (
            <div className="card" key={current.id}>
                <NavLink to={"/certificateProduct/" + current.id}>
                    <img className="background" src={"/public/img/certificates/" + current.imageSrc} alt=""/>
                    <div className="content">
                        <div className="info">
                            <div className="place">
                                <img src="/public/img/marker.png" alt=""/>
                                <p>{current.town}</p>
                            </div>
                            <div className="duration">
                                <img src="/public/img/clock.png" alt=""/>
                                <p>{current.time}</p>
                            </div>
                        </div>
                    </div>
                    <div className="footer-info">
                        <div className="title">
                            {current.title}
                        </div>
                        <div className="price">
                            {current.price}
                        </div>
                    </div>
                </NavLink>
            </div>
        )
    });

    return (
        <div className="">
            <div className="certificate-product">
                <div className="wrapper">
                    <div className="content">
                        <img src={"/public/img/certificates/" + product.dopImage} alt=""/>
                        <div className="info">
                            <div className="title">Описание сертификата</div>
                            <div className="description" dangerouslySetInnerHTML={description}></div>
                        </div>
                    </div>
                    <div className="buy">
                        <div className="price">{product.price}</div>
                        <div className="button" onClick={() => {buy(props.store, product)}}>Купить</div>
                    </div>
                </div>
            </div>
            <div className="recommended-certificates">
                <div className="title">Может быть интересно</div>
                <div className="recommended-events-list">
                    {recommended}
                </div>
                <NavLink to={'/certificates'}>
                    <div className="btn-container"><div className="button">Больше сертификатов</div></div>
                </NavLink>
            </div>
        </div>
    )
}

export default CertificateProduct;