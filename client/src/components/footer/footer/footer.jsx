import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
  
import './footer.scss';

function Foot(props) {
    return (
        <div className="footer">
            <div className="container">
                <div className="navbar">
                    <div className="">Новости</div>
                    <div className=""></div>
                    <div className="">Контакты</div>
                    <div className=""></div>
                    <div className="">О нас</div>
                    <div className=""></div>
                    <NavLink to={'/events'}>
                        <div className="">События</div>
                    </NavLink>
                    <div className=""></div>
                    <NavLink to={'/certificates'}>
                        <div className="">Сертификаты</div>
                    </NavLink>
                    <div className=""></div>
                    <div className="">Корпоративы</div>
                </div>
                <div className="line"></div>
                <div className="content">
                    <div className="personal-info">
                        <img src="/public/img/footer.png" alt=""/>
                        <p>© 2020 bel.by</p>
                        <p>Белорусский клуб активного отдыха</p>
                    </div>
                    <div className="town-info">
                        <p><img src="/public/img/town.png" alt=""/></p>
                        <p className="title">Гомель</p>
                        <p>Гомель ул. Мазурова 127а</p>
                        <p>+375 44 70-30-226</p>
                        <p><a href="mailto:dmitriy.brusnev@mail.ru">dmitriy.brusnev@mail.ru</a></p>
                    </div>
                    <div className="links-info">
                        <p className="title">Мы в соц. сетях</p>
                        <p>
                            <a href="">
                                <img src="/public/img/facebook.png" alt=""/>
                            </a>
                            <a href="">
                                <img src="/public/img/instagram.png" alt=""/>
                            </a>
                        </p>
                        <p><a href="">Политика конфиденциальности</a></p>
                        <p><a href="">Договор публичной оферты</a></p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Foot;