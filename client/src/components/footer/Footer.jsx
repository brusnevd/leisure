import React from 'react';
import Foot from './footer/Footer';
import Info from './info/Info';
import References from './references/References';

function Footer(props) {
    return (
        <div className="footer">
            <References />
            <Info />
            <Foot />
        </div>
    )
}

export default Footer;