import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
import './references.scss';

function References(props) {
    return (
        <div className="references">
            <div className="block camping">
                <div className="container">
                    {/* <img src="/public/img/camping.png" alt=""/> */}
                    <p className="title">Прокат снаряжения</p>
                    <p className="subtitle">Собираясь в туристический поход, следует взять с собой все необходимое, и самое главное - палатку, рюкзак, спальник и коврик. Без этих предметов, естественно, никакой турист не сможет провести и дня на природе, особенно когда погодные условия не слишком комфортные. Если в наличии этих предметов нет, можно использовать прокат снаряжения как альтернативу его непосредственной покупки.</p>
                    <NavLink to={'/events'}>
                        <div className="button">Подробнее</div>
                    </NavLink>
                </div>
            </div>
            <div className="block party">
                <div className="container">
                    {/* <img src="/public/img/confety.png" alt=""/> */}
                    <p className="title">Организация корпоративов</p>
                    <p className="subtitle">Наша команда предлагает Вашему вниманию активные, захватывающие идеи и уникальные варианты для вашего события. С нами Ваше мероприятие будет ярким, увлекательным и запоминающимся! Креативная команда, сумасшедшие идеи и большой спектр услуг, которые помогут дополнить Ваш праздник новыми впечатлениями и эмоциями!</p>
                    <NavLink to={'/events'}>
                        <div className="button">Подробнее</div>
                    </NavLink>
                </div>
            </div>
            <div className="block skiing">
                <div className="container">
                    {/* <img src="/public/img/compass.png" alt=""/> */}
                    <p className="title">Организация событий</p>
                    <p className="subtitle">Мы проводим любые варианты экстремального отдыха в Днепре и не только, отдых в Карпатах, восхождение на гору, прогулки по лесу, трекинг Турция, поездки на берег моря, покатать на лыжах или сноуборде и многое другое! Также мы знаем где и как полетать на параплане, прыгнуть с веревкой, прыгнуть с парашютом и полетать на самолёте.</p>
                    <NavLink to={'/events'}>
                        <div className="button">Подробнее</div>
                    </NavLink>
                </div>
            </div>
        </div>
    )
}

export default References;