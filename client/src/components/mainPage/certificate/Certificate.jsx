import React, { useState, useEffect, useCallback } from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
import './certificate.scss';

function Certificate(props) {
    // Здесь будет браться информация из БД
    const [isLoaded, setIsLoaded] = useState(false);

    // Посылаем запрос на сервер
    useEffect(() => {
        fetch("http://localhost:3333/certificates")
            .then(res => res.json())
            .then(
                (result) => {
                    props.setCertificates(result);
                    setIsLoaded(true);
                },
                (error) => {
                    console.log(error)
                    setIsLoaded(true);
                }
            )
    }, [])

    if (!isLoaded) {
        return (
            <div className="">Загрузка</div>
        )
    } else {
        let state = props.store.getState().certificates;

        let cards = state.map((current) => {
            return (
                <div className="card" key={current.id}>
                    <NavLink to={"/certificateProduct/" + current.id}>
                        <img className="background" src={"/public/img/certificates/" + current.imageSrc} alt=""/>
                        <div className="content">
                            <div className="info">
                                <div className="place">
                                    <img src="/public/img/marker.png" alt=""/>
                                    <p>{current.town}</p>
                                </div>
                                <div className="duration">
                                    <img src="/public/img/clock.png" alt=""/>
                                    <p>{current.time}</p>
                                </div>
                            </div>
                        </div>
                        <div className="footer-info">
                            <div className="title">
                                {current.title}
                            </div>
                            <div className="price">
                                {current.price}
                            </div>
                        </div>
                    </NavLink>
                </div>
            )
        });

        if (cards.length > 8) {
            cards = cards.slice(0, 8);
        }

        return (
            <div className="certificates">
                <div className="title">Подарочные сертификаты</div>
                <div className="events-list">
                    <div className="left-side">{[cards[0]]}</div>
                    <div className="right-side">{[cards[3], cards[2]]}</div>
                </div>
                <NavLink to={'/certificates'}>
                    <div className="btn-container"><div className="button">Больше сертификатов</div></div>
                </NavLink>
            </div>
        )
    }
}

export default Certificate;