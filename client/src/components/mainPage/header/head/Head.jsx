import React from 'react';
import showBasket from '../../../../custom/basket';

function Head(props) {
    return (
        <div className="header-head">
            <div className="left-side">
                <div className="language">
                    <div className="language-inner">RU</div>
                </div>
                <div className="vertical-line"></div>
                <div className="town-select">
                    <img src="/public/img/map-marker.png" alt=""/>
                    Гомель
                </div>
            </div>

            <div className="main">
                <span className="stand"></span>
                <div className="cat">
                    <div className="body"></div>
                    <div className="head">
                        <div className="ear"></div>
                        <div className="ear"></div>
                    </div>
                    <div className="face">
                        <div className="nose"></div>
                        <div className="whisker-container">
                            <div className="whisker"></div>
                            <div className="whisker"></div>
                        </div>
                        <div className="whisker-container">
                            <div className="whisker"></div>
                            <div className="whisker"></div>
                        </div>
                    </div>
                    <div className="tail-container">
                        <div className="tail">
                            <div className="tail">
                                <div className="tail">
                                    <div className="tail">
                                    <div className="tail">
                                        <div className="tail">
                                            <div className="tail"></div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="right-side">
                <div className="phone">
                    <img src="/public/img/phone.png" alt=""/>
                    +375 44 70-30-226
                </div>
                <div className="vertical-line"></div>
                <div className="basket" onClick={() => {showBasket(props.store)}}>
                    <div className="count" id="basket-length">0</div>
                    <img src="/public/img/basket.png" alt=""/>
                </div>
            </div>
        </div>
    )
}

export default Head;