import React from 'react';
import {
    BrowserRouter as Router,
    Route,
  } from "react-router-dom";
import Body from './body/Body';
import Head from './head/Head';
import EventsHead from '../../eventsPage/eventshead';
import "./header.scss";
import "./cat.scss";
import EventProductHead from '../../eventsPage/EventProductHead';
import CertificatesHead from '../../certificatesPage/CertificatesHead';
import CertificateProductHead from '../../certificatesPage/CertificateProductHead';

function Header(props) {
    return (
        <div className="header">
            <Head {...props} />
            <Route path='/' exact render={(propses) => <Body {...propses} />}></Route>
            <Route path='/events' exact render={(propses) => <EventsHead {...propses} {...props} />}></Route>
            <Route path='/certificates' exact render={(propses) => <CertificatesHead {...propses} {...props} />}></Route>
            <Route path='/eventProduct/:id' exact render={(propses) => <EventProductHead {...propses} {...props} />}></Route>
            <Route path='/certificateProduct/:id' exact render={(propses) => <CertificateProductHead {...propses} {...props} />}></Route>
        </div>
    )
}

export default Header;