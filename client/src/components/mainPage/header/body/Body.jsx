import React from 'react';
import { NavLink } from 'react-router-dom';

function Body(props) {
    return (
        <div className="header-body">
            <div className="left-side">
                <p className="btn-title">Новости</p>
                <div className="btn-events header-btn">
                    <img src="/public/img/news.png" alt=""/>
                </div>
                <div className="circle"></div>
                <div className="circle"></div>
                <div className="circle"></div>
                <p className="btn-title">Контакты</p>
                <div className="btn-certificates header-btn">
                    <img src="/public/img/contacts.png" alt=""/>
                </div>
                <div className="circle"></div>
                <div className="circle"></div>
                <div className="circle"></div>
                <p className="btn-title">О нас</p>
                <div className="btn-corporates header-btn">
                    <img src="/public/img/about.png" alt="" className="less-size"/>
                </div>
            </div>
            <div className="middle-side">
                <img src="/public/img/logo.png" alt=""/>
                <p className="title">Активный отдых и экстрим<br></br> с BelLeisure</p>
                <div className="about">
                    <div className="towns">
                        <img src="/public/img/towns.png" alt=""/>
                        <p className="title">6 городов</p>
                    </div>
                    <div className="vertical-line"></div>
                    <div className="people">
                        <img src="/public/img/people.png" alt=""/>
                        <p className="title">5000 клиентов</p>
                    </div>
                </div>
            </div>
            <div className="right-side">
                <p className="btn-title">События</p>
                <NavLink to={'/events'}>
                    <div className="btn-events header-btn">
                        <img src="/public/img/events.png" alt="" className="less-size"/>
                    </div>
                </NavLink>
                <div className="circle"></div>
                <div className="circle"></div>
                <div className="circle"></div>
                <p className="btn-title">Сертификаты</p>
                <NavLink to={'/certificates'}>
                    <div className="btn-certificates header-btn">
                        <img src="/public/img/certificates.png" alt=""/>
                    </div>
                </NavLink>
                <div className="circle"></div>
                <div className="circle"></div>
                <div className="circle"></div>
                <p className="btn-title">Корпоративы</p>
                <div className="btn-corporates header-btn">
                    <img src="/public/img/corporates.png" alt="" className="less-size"/>
                </div>
            </div>
        </div>
    )
}

export default Body;