import React, { useState, useEffect, useCallback } from 'react';
import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
import './event.scss';

function Event(props) {
    // Здесь будет браться информация из БД
    const [isLoaded, setIsLoaded] = useState(false);

    // Посылаем запрос на сервер
    useEffect(() => {
        fetch("http://localhost:3333/")
            .then(res => res.json())
            .then(
                (result) => {
                    props.setEvents(result);
                    setIsLoaded(true);
                },
                (error) => {
                    console.log(error)
                    setIsLoaded(true);
                }
            )
    }, [])

    if (!isLoaded) {
        return (
            <div className="">Загрузка</div>
        )
    } else {
        let state = props.store.getState().events;

        let cards = state.map((current) => {
            return (
                <div className="card" key={current.id}>
                    {/* <div className="image"> */}
                        <img src={"/public/img/events/" + current.imageSrc} alt=""/>
                    {/* </div> */}
                    <p className="title">{current.title}</p>
                    <div className="info">
                        <div className="place">
                            <img src="/public/img/map-marker.png" alt=""/>
                                <p>{current.place}</p>
                        </div>
                        <div className="count">
                            <img src="/public/img/person.png" alt=""/>
                            <p>Количество мест - {current.count}</p>
                        </div>
                        <div className="duration">
                            <img src="/public/img/clock.png" alt=""/>
                            <p>{current.time}</p>
                        </div>
                        <div className="date">
                            <img src="/public/img/calendar.png" alt=""/>
                            <p>{current.date}</p>
                        </div>
                    </div>
                    <div className="footer-info">
                        <div className="price">{current.price}</div>
                        <NavLink to={'/eventProduct/' + current.id}>
                            <div className="footer-button">Подробнее</div>
                        </NavLink>
                    </div>
                </div>
            )
        });

        if (cards.length > 8) {
            cards = cards.slice(0, 8);
        }

        return (
            <div className="events">
                <div className="title">События</div>
                <div className="events-list">
                    {/* Вместо того, что ниже будет раскрывать массив с информацией из БД и создавать карточки */}
                    {cards}
                </div>
                <NavLink to={'/events'}>
                    <div className="btn-container"><div className="button">Больше событий</div></div>
                </NavLink>
            </div>
        )
    }
}

export default Event;