import React, { useState, useEffect } from 'react';
import {connect} from "react-redux";
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import EventProduct from './components/eventsPage/EventProduct';
import Events from './components/eventsPage/Events';
import Footer from './components/footer/Footer';
import Event from './components/mainPage/event/event';
import Header from './components/mainPage/header/Header';
import Certificates from './components/certificatesPage/Certificates';
import ScrollToTop from './components/scrollToTop';

import { setEvents, setCertificates } from './redux/actions'
import CertificateProduct from './components/certificatesPage/CertificateProduct';
import Certificate from './components/mainPage/certificate/Certificate';

function App(props) {
  return (
    <Router>
        <ScrollToTop />
        <Header {...props} />
        <Route path='/' exact render={(propses) => <Certificate {...propses} {...props} />}></Route>
        <Route path='/' exact render={(propses) => <Event {...propses} {...props} />}></Route>
        <Route path='/events' exact render={(propses) => <Events {...props} {...propses} />} ></Route>
        <Route path='/certificates' exact render={(propses) => <Certificates {...props} {...propses} />} ></Route>
        <Route path='/eventProduct/:id' exact render={(propses) => <EventProduct {...props} {...propses} />} ></Route>
        <Route path='/certificateProduct/:id' exact render={(propses) => <CertificateProduct {...props} {...propses} />} ></Route>
        <Footer />
    </Router>
  );
}

function mapStateToProps(state) {
  return state;
}

let mapDispatchToProps = {
  setEvents,
  setCertificates
}

export default connect(mapStateToProps, mapDispatchToProps)(App);