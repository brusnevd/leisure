import { setBasket } from "../redux/actions";

export default function buy(store, item) {
    let flag = 0;
    let basket = store.getState().basket.slice();
    basket = basket.map(cur => {
        if (cur.id == item.id && cur.title == item.title) {
            let copy = {};
            Object.assign(copy, cur);
            console.log("cur - ", cur);
            copy.price = parseFloat(cur.price) + parseFloat(item.price) + " р.";
            flag = 1;
            return copy;
        } else {
            return cur;
        }
    });
    if (flag == 0) {
        basket.push(item);
    }
    store.dispatch(setBasket(basket));

    document.getElementById("basket-length").innerText = basket.length;

    let modal = document.createElement("div");
    modal.classList.add("access-buy");
    modal.innerText = "Добавлено в корзину";
    modal.style.opacity = "1";

    document.body.style.position = "relative";
    document.body.appendChild(modal);

    let animate = setInterval(() => {
        let opacity = modal.style.opacity;
        modal.style.opacity = Number(opacity) - 0.01;
        if (opacity == 0) {
            clearInterval(animate);
            document.body.removeChild(modal);
        }
    }, 20);
}