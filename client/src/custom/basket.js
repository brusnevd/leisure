import { setBasket } from "../redux/actions";

export default function showBasket(store) {
    let basket = store.getState().basket;
    console.log("basket - ", basket);

    document.body.classList.add("active-basket");

    let wrapper = document.createElement("div");
    wrapper.classList.add("modal-wrapper");

    let modal = document.createElement("div");
    modal.classList.add("modal-basket");
    modal.id = "modal-basket";

    let header = document.createElement("div");
    header.classList.add("basket-header");

    let closebasket = document.createElement("span");
    closebasket.classList.add("close-basket");
    closebasket.innerHTML = "&#10006;";

    closebasket.onclick = () => {
        let animate = setInterval(() => {
            wrapper.style.opacity = Number(wrapper.style.opacity) - 0.05;
            if (wrapper.style.opacity == 0) {
                document.body.classList.remove("active-basket");
                document.body.removeChild(wrapper);
                clearInterval(animate);
            }
        }, 3);
    }

    // modal.appendChild(close);
    header.appendChild(closebasket);

    let logo = document.createElement("img");
    logo.src = "/public/img/logo.png";

    header.appendChild(logo);
    // modal.appendChild(logo);

    let titlebasket = document.createElement("div");
    titlebasket.classList.add("title");
    if (basket.length != 0) {
        titlebasket.innerText = `В корзине ${basket.length} ед.`;
    } else {
        titlebasket.innerText = "В корзине ничего нет";
    }

    header.appendChild(titlebasket);
    // modal.appendChild(title);

    modal.appendChild(header);

    basket.forEach(current => {
        let item = document.createElement("div");
        item.classList.add("item");
        item.id = "basketitem_" + current.id;

        let image = document.createElement("img");
        if (current.date == "Любой" || current.date == "Лето" || current.date == "Осень" || current.date == "Зима" || current.date == "Весна") {
            image.src = "/public/img/certificates/" + current.imageSrc;
        } else {
            image.src = "/public/img/events/" + current.imageSrc;
        }

        let info = document.createElement("div");
        info.classList.add("info");
        let title = document.createElement("div");
        title.classList.add("item-title");
        title.innerText = current.title;
        title.id = "title_" + current.id;
        let price = document.createElement("div");
        price.classList.add("price");
        price.innerText = current.price;

        info.appendChild(title);
        info.appendChild(price);

        let close = document.createElement("div");
        close.innerHTML = "&#10006;";
        close.classList.add("delete");
        close.id = "delete_" + current.id;

        close.addEventListener("click", (event) => {
            let basket = store.getState().basket.slice();
            let elemid = event.target.id.split("_")[1];
            let elemtitle = document.getElementById("title_" + elemid).innerText;
            basket = basket.filter(cur => {
                if (cur.id == elemid && cur.title == elemtitle) {
                    return false;
                } else {
                    return true;
                }
            });
            store.dispatch(setBasket(basket));
            modal.removeChild(document.getElementById("basketitem_" + elemid));
            if (basket.length == 0) {
                closebasket.click();
            }
            titlebasket.innerText = `В корзине ${basket.length} ед.`;
            document.getElementById("basket-length").innerText = basket.length;
        });

        item.appendChild(image);
        item.appendChild(info);
        item.appendChild(close);

        modal.appendChild(item);
    });

    let form = document.createElement("div");
    form.classList.add("order");

    let formin = document.createElement("div");
    formin.classList.add("form");

    let name = document.createElement("input");
    name.classList.add("form-name");
    name.placeholder = "Ваше имя";
    let phone = document.createElement("input");
    phone.placeholder = "Ваш телефон";
    phone.classList.add("form-phone");
    let button = document.createElement("div");
    button.classList.add("button");
    button.innerText = "Оформить заказ";

    formin.appendChild(name);
    formin.appendChild(phone);
    formin.appendChild(button);


    button.onclick = function() {
        if (name.value.length != 0 && phone.value.length != 0) {
            let container = document.createElement("div");
            container.classList.add("loading");
            for (let i = 1; i < 6; i++) {
                let circle = document.createElement("div");
                circle.classList.add("circle");
                circle.classList.add("circle-" + i);
                container.appendChild(circle);
            }
            container.style.position = "fixed";
            container.style.zIndex = "999999";
            document.body.style.position = "relative";
            document.body.prepend(container);
            fetch("http://localhost:3333/buy/", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8'
                    },
                    body: JSON.stringify([basket, name.value, phone.value])
                })
                .then(
                    () => {
                        document.body.removeChild(container);
                        closebasket.click();
                        let modal = document.createElement("div");
                        modal.classList.add("access-buy");
                        modal.classList.add("access");
                        modal.innerText = "Мы вам позвоним в ближайшее время!";
                        modal.style.opacity = "1";

                        document.body.style.position = "relative";
                        document.body.appendChild(modal);

                        setTimeout(() => {
                            let animate = setInterval(() => {
                                let opacity = modal.style.opacity;
                                modal.style.opacity = Number(opacity) - 0.01;
                                if (opacity == 0) {
                                    clearInterval(animate);
                                    document.body.removeChild(modal);
                                }
                            }, 20);
                        }, 1000);
                    },
                    (error) => {
                        console.log(error);
                        document.body.removeChild(container);
                    }
                )
        }
    }

    form.appendChild(formin);
    modal.appendChild(form);

    wrapper.appendChild(modal);

    wrapper.style.opacity = "0";
    document.body.prepend(wrapper);

    let animate = setInterval(() => {
        wrapper.style.opacity = Number(wrapper.style.opacity) + 0.05;
        if (wrapper.style.opacity == 1) {
            clearInterval(animate);
        }
    }, 3);
}