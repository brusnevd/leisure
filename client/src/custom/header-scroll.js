window.addEventListener("scroll", () => {
    const header = document.querySelector(".header-head");
    if (pageYOffset == 0) {
        header.classList.remove("scroll");
    } else {
        header.classList.add("scroll");
    }
})